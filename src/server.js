import express from "express";
import cors from "cors";
import mongoose from "mongoose";
import env from "dotenv";
import userRoutes from "./routes/user.js";

const app = express();

app.use(express.json());
app.use(cors());
env.config();
app.use(userRoutes);

//Routes

//conexión a bd
mongoose
  .connect(process.env.URI_MONGODB)
  .then(() => console.log("Conexión exitosa con Atlas"))
  .catch((error) => console.error(error));

const port = 3000;

app.listen(port, () => {
  console.log(`Servidor en el puerto ${port}`);
});
