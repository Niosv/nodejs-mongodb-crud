import User from "../models/user.js";

export const getAllUsers = async (req, res) => {
  try {
    const users = await User.find();
    res.status(200).json(users);
  } catch (error) {
    res.status(500).json({ message: error });
  }
};

export const getUserById = async (req, res) => {
  try {
    const id = req.params.id;
    const user = await User.findd({ _id: id });
    if (user.length === 0) {
      return res.status(404).json({ message: "No existe este usuario" });
    }
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json({ message: error });
  }
};

export const getUserByEmail = async (req, res) => {
  try {
    const email = req.params.email;
    const user = await User.find({ email: email });
    if (user.length === 0) {
      return res
        .status(404)
        .json({ message: "No existe un usuario con este email" });
    }
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

//Agrega un usuario a la base de datos
export const addUser = async (req, res) => {
  try {
    const { name, age, email, password } = req.body;
    const newUser = new User({
      name: name,
      age: age,
      email: email,
      password: password,
    });
    console.log(req.body);
    console.log(newUser);
    await newUser
      .save()
      .then(() => console.log(`${name} ha sido agregado como nuevo usuario`));
    res.status(201).json(newUser);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

export const updateUser = async (req, res) => {
  try {
    const id = req.params.id;
    const changes = req.body;
    User.findOneAndUpdate({ _id: id }, changes)
      .then(() => console.log("Usuario actualizado"))
      .catch((err) => console.error(err));
    res.status(200).json({ message: "Usuario actualizado" });
  } catch (error) {
    res.status(500).json({ message: error });
  }
};

export const deleteUser = async (req, res) => {
  try {
    const id = req.params.id;
    User.deleteOne({ _id: id })
      .then(() => console.log("Usuario eliminado"))
      .catch((err) => console.error(err));
    res.status(200).json({ message: "Usuario eliminado" });
  } catch (error) {
    res.status(500).json({ message: error });
  }
};
