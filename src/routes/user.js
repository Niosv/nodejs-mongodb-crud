import express from "express";
import {
  addUser,
  deleteUser,
  getAllUsers,
  getUserByEmail,
  getUserById,
  updateUser,
} from "../controllers/user.js";

const router = express.Router();

router.get("/users", getAllUsers);
router.get("/userById/:id", getUserById);
router.get("/userByEmail/:email", getUserByEmail);
router.post("/users", addUser);
router.put("/user/:id", updateUser);
router.delete("/user/:id", deleteUser);

export default router;
